﻿using EFTest.DAL;
using EFTest.Domain;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MyAPI.Controllers
{
    public class ValuesController : ApiController
    {
        // GET api/values
        public List<StudentVM> Get()
        {
            List<StudentVM> students;
            using (var context = new SchoolContext())
            { 
                students = context.Students.Select(s => new StudentVM { Name = s.Name, MobileNumber =  s.MobileNumber }).ToList();
            }
            return students;
        }

        // GET api/values/5
        public object Get(int id)
        {
            object student;
            using (var context = new SchoolContext())
            {
                var query = context.Students.Where(st => st.ID == id).Select(s => new { s.Name });
                student = query.First();                
            }
            return student;
        }

        // POST api/values
        public void Post([FromBody]Student student)
        {
            using (var context = new SchoolContext())
            {
                context.Students.Add(student);
                context.SaveChanges();
            }              
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]Student student)
        {
            using (var context = new SchoolContext())
            {
                var s = context.Students.First(s1 => s1.ID == id);
                s.MobileNumber = student.MobileNumber;
                context.SaveChanges();
            }
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
            using (var context = new SchoolContext())
            {
                var s = context.Students.First(s1 => s1.ID == id);
                context.Students.Remove(s);
                context.SaveChanges();
            }
        }
    }
}
