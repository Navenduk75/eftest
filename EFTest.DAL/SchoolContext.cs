﻿using EFTest.Domain;
using System.Data.Entity;

namespace EFTest.DAL
{
    public class SchoolContext : DbContext
    {
        public SchoolContext(): base()
        {
        }

        public DbSet<Student> Students { get; set; }
        public DbSet<Grade> Grades { get; set; }
    }
}
