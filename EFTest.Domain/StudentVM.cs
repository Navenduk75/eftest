﻿namespace EFTest.Domain
{
    public class StudentVM
    {
        public string Name { get; set; }
        public int MobileNumber { get; set; }
    }
}
