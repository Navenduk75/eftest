﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFTest.Domain
{
    public class Student
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int MobileNumber { get; set; }

        public Grade Grade { get; set; }
    }
}
